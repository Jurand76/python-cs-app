import json
import os


def message_new(data, server, user_logged, users_list):
    parts = data[11:].split(',', 1)
    messages_read = {}

    if user_logged == 'none':
        response = "You need to log in to send messages"
        print('No user logged to send messages')
    else:
        try:
            username_dest = parts[0].replace(" ", "")
            message_text = parts[1]
            if message_text.startswith(" "):
                message_text = message_text.replace(" ", "", 1)

            print(f'Message to user: {username_dest}')
            print(f'Text: {message_text}')

            if username_dest in users_list:
                try:
                    command = f"INSERT INTO messages (userfrom, userto, messagetext, read) VALUES ('{user_logged}', '{username_dest}', '{message_text}', False)"
                    db_response = server.db_write(command)
                    response = f"Successfully sent to {username_dest}"
                except FileNotFoundError:
                    print(f'Cannot send message to user {username_dest}')
            else:
                response = f"User {username_dest} not exists in database"
        except NotImplementedError:
            response = "Bad parameters of command"

    return response


def message_delete(data, server, user_logged, user_admin):
    username_delete = data[15:].replace(" ", "")

    if user_logged == 'none':
        response = "You need to log in to delete messages"
        return response

    if user_logged != username_delete and user_admin is not True:
        response = "You cannot delete other users messages - administrator privileges needed"
        return response

    try:
        command = 'DELETE FROM messages WHERE userto = '
        command = command + "'" + username_delete + "'"
        db_response = server.db_write(command)
        if db_response == "Executed":
            response = f"Deleted messages for: {username_delete}"
        else:
            response = f"Cannot delete messages for: {username_delete}"
    except NotImplementedError:
        response = "Bad parameters of command"

    return response


def message_read(data, user_logged, messages):
    number = int(data[12:].replace(" ", ""))

    if user_logged == 'none':
        response = "You need to log in to read messages"
        return response

    if messages == {}:
        response = f"No messages for user {user_logged}"
        return response
    else:
        print(messages)

    if number in messages:
        response = f"Message from {messages[number]['from']}: {messages[number]['message']}"
    else:
        response = "Bad number of message to read"

    return response
