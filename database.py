import psycopg2
import logindata


class DBServer:
    def __init__(self):
        # AWS RDS Database Credentials
        self.DB_HOST = 'python-z2j-postgresql.cyw7sqz5hk8r.eu-west-2.rds.amazonaws.com'
        self.DB_USER = 'tjarnot'
        self.DB_PASSWORD = logindata.getpass(self.DB_USER)
        self.DB_NAME = 'PythonCS'  # database name
        self.DB_PORT = 5432  # port for PostgreSQL
        self.connection = psycopg2.connect(
            host=self.DB_HOST,
            user=self.DB_USER,
            password=self.DB_PASSWORD,
            dbname=self.DB_NAME,
            port=self.DB_PORT
        )

    def db_server_start(self):
        connection = self.connection

        print('Connected to PostgreSQL AWS RDS Server')

        # creating tables - users and messages
        with connection.cursor() as cursor:
            # Create a new record
            try:
                cursor.execute('''
                    CREATE TABLE users (
                        id SERIAL PRIMARY KEY,
                        username VARCHAR(255) NOT NULL UNIQUE,
                        password VARCHAR(255) NOT NULL,
                        admin_privileges BOOLEAN NOT NULL DEFAULT FALSE
                    );
                ''')
                connection.commit()
            except psycopg2.DatabaseError:
                print('Table users exists')
                connection.rollback()

            try:
                cursor.execute('''
                    CREATE TABLE messages (
                        id SERIAL PRIMARY KEY,
                        userfrom VARCHAR(255) NOT NULL,
                        userto VARCHAR(255) NOT NULL,
                        messagetext VARCHAR(255) NOT NULL,
                        read BOOLEAN NOT NULL DEFAULT FALSE
                    );
                ''')
                connection.commit()
            except psycopg2.DatabaseError:
                print('Table messages exists')
                connection.rollback()

    def db_read(self, command):
        connection = self.connection

        with connection.cursor() as cursor:
            # Execute PostgreSQL read command
            try:
                cursor.execute(command)
                connection.commit()
                print(f'Database reply - trying to read from command "{command}"')
                try:
                    response = cursor.fetchall()
                    return response
                except NotImplementedError:
                    return "No response data"
            except psycopg2.DatabaseError:
                print(f'Database reply - command "{command}" not executed')
                connection.rollback()
                return "Not executed"

    def db_write(self, command):
        connection = self.connection

        with connection.cursor() as cursor:
            # Execute PostgreSQL write ommand
            try:
                cursor.execute(command)
                connection.commit()
                print(f'Database reply - trying to write from command "{command}"')
                return "Executed"
            except psycopg2.DatabaseError:
                print(f'Database reply - command "{command}" not executed')
                connection.rollback()
                return "Not executed"

    def db_server_close(self):
        connection = self.connection
        connection.close()
        print('Server AWS PostreSQL has been stopped')
