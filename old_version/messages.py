import json
import users
import os


def message_new(data, user_logged, users_list):
    parts = data[11:].split(',', 1)
    messages_read = {}

    if user_logged == 'none':
        response = "You need to log in to send messages"
        print('No user logged to send messages')
    else:
        try:
            username_dest = parts[0].replace(" ", "")
            message_text = parts[1]
            if message_text.startswith(" "):
                message_text = message_text.replace(" ", "", 1)

            print(f'Message to user: {username_dest}')
            print(f'Text: {message_text}')

            filename = username_dest + '.json'

            if username_dest in users_list:
                try:
                    with open(filename, 'r') as file:
                        print('Message file loaded')
                        messages_read = json.load(file)
                except FileNotFoundError:
                    print('No message file')
                    messages_read = {}

                if len(messages_read) < 5:
                    messages_read[len(messages_read) + 1] = {'from': user_logged, 'text': message_text}
                    try:
                        with open(filename, 'w') as file:
                            json.dump(messages_read, file)
                            response = f"Message sent from {user_logged} to {username_dest}"
                            print('Message successfully sent')
                    except FileNotFoundError:
                        response = f"Cannot create file with name {filename}"
                else:
                    response = f"User {username_dest} have full inbox"
            else:
                response = f"User {username_dest} not exists in database"
        except NotImplementedError:
            response = "Bad parameters of command"

    return response


def message_delete(data, user_logged, user_admin):
    username_delete = data[15:].replace(" ", "")

    if user_logged == 'none':
        response = "You need to log in to delete messages"
        return response

    if user_logged != username_delete and user_admin != 'yes':
        response = "You cannot delete other users messages - administrator privileges needed"
        return response

    try:
        filename = username_delete + '.json'
        if os.path.exists(filename):
            os.remove(filename)
            print(f"{filename} has been deleted")
            response = f"Messages for user {username_delete} has been removed"
        else:
            response = f"No file with messages for user {username_delete}"

    except FileNotFoundError:
        response = "Bad parameters of command"

    return response


def message_read(data, user_logged):
    number = data[12:].replace(" ", "")

    if user_logged == 'none':
        response = "You need to log in to read messages"
        return response

    try:
        messages = users.user_messages_load(user_logged)
    except FileNotFoundError:
        response = "Bad parameters of command"
        return response

    if messages == {}:
        response = f"No messages for user {user_logged}"
        return response

    if number in messages:
        response = f"Message from {messages[number]['from']}: {messages[number]['text']}"
    else:
        response = "Bad number of message to read"

    return response
