import json
import socket
import users
import messages
from datetime import datetime


class Server:
    def __init__(self):
        self.HOST = "127.0.0.1"
        self.PORT = 65431
        self.start_time = datetime.now()
        self.VERSION = str(
            f"Server at host {self.HOST} working on port {self.PORT}. Software version 1.0. Server started {self.start_time}")
        self.users = {}
        self.users_file = 'users.json'
        self.user_logged = 'none'
        self.user_admin = 'no'
        self.messages = {}

    def get_server_time(self):
        time_now = datetime.now()
        return time_now - self.start_time

    def help_command(self):
        help_data = {"uptime": "zwraca czas zycia serwera",
                     "info": "zwraca numer wersji serwera, date jego utworzenia",
                     "help": "zwraca liste dostepnych komend",
                     "stop": "zatrzymuje jednoczesnie serwer i klienta",
                     "user_new name, password, no/yes": "dodaje nowego użytkownika (no/yes - admin privileges)",
                     "user_delete name": "usuwa użytkownika - wymagane prawa administratora",
                     "user_list": "wyświetla listę nazw użytkowników",
                     "user_log name, password": "logowanie użytkownika",
                     "user_message": "wyświetla liczbę wiadomości dla zalogowane użytkownika",
                     "message_read number": "wyświetla wiadomość o numerze number",
                     "message_new user, text": "nowa wiadomość do użytkownika user",
                     "messages_delete user": "kasowanie skrzynki wiadomości użytkownika user"}
        return help_data

    def server_answer(self, connection, response):
        connection.sendall(json.dumps(response).encode('utf-8'))

    def server_start(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.HOST, self.PORT))
            s.listen()
            self.users = users.load_users(self.users_file)
            conn, addr = s.accept()
            with conn:
                print(f"Connected by {addr}")
                while True:
                    data = conn.recv(1024).decode('utf8')
                    print(f"Received data: {data}")
                    good_data = False
                    if data == "info":
                        response = self.VERSION
                        self.server_answer(conn, response)
                        good_data = True
                    if data == "uptime":
                        response = str(self.get_server_time())
                        self.server_answer(conn, response)
                        good_data = True
                    if data == "help":
                        response = self.help_command()
                        self.server_answer(conn, response)
                        good_data = True
                    if data == "stop":
                        response = "Server stopped by user"
                        self.server_answer(conn, response)
                        good_data = True
                        break
                    if data == "user list" or data == "user_list":
                        response = users.user_list(self.users)
                        self.server_answer(conn, response)
                        good_data = True
                    if data.lower().startswith('user new') or data.lower().startswith('user_new'):
                        response = users.user_new(data, self.users, self.users_file)
                        self.server_answer(conn, response)
                        good_data = True
                    if data.lower().startswith('user log') or data.lower().startswith('user_log'):
                        response = users.user_log(data, self.users)
                        if not response.startswith("Cannot"):
                            self.user_logged = response
                            self.user_admin = self.users[self.user_logged]['admin']
                            self.messages = users.user_messages_load(self.user_logged)
                            response = response + " successfully logged. "
                            if self.user_admin == 'yes':
                                response = response + "Has administration privileges."
                        self.server_answer(conn, response)
                        good_data = True
                    if data.lower().startswith('user delete') or data.lower().startswith('user_delete'):
                        response = users.user_delete(data, self.users_file, self.users, self.user_admin)
                        self.server_answer(conn, response)
                        good_data = True
                    if data.lower().startswith('user messages') or data.lower().startswith('user_messages'):
                        response = users.user_messages(self.user_logged, self.messages)
                        self.server_answer(conn, response)
                        good_data = True
                    if data.lower().startswith('message new') or data.lower().startswith('message_new'):
                        response = messages.message_new(data, self.user_logged, self.users)
                        self.server_answer(conn, response)
                        good_data = True
                    if data.lower().startswith('messages delete') or data.lower().startswith('messages_delete'):
                        response = messages.message_delete(data, self.user_logged, self.user_admin)
                        self.server_answer(conn, response)
                        good_data = True
                    if data.lower().startswith('message read') or data.lower().startswith('message_read'):
                        response = messages.message_read(data, self.user_logged)
                        self.server_answer(conn, response)
                        good_data = True
                    if not good_data:
                        response = "Bad command"
                        self.server_answer(conn, response)


server = Server()
server.server_start()
