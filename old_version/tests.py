import unittest
from users import *
from messages import *


class TestUser(unittest.TestCase):

    def setUp(self):
        self.users_list = {}
        self.users_list_full = {"Janina": {"password": "abcd", "admin": "no"},
                                "Zdzislaw": {"password": "bcde", "admin": "yes"}}
        self.first_user = "Janina"
        self.first_passw = "abcd"
        self.second_user = "Zdzislaw"
        self.second_passw = "bcde"
        self.user_bad = "Xybyz"
        self.messages = {}
        self.filename = "test_users.json"

    def test_new_user(self):
        result = "User created successfully."
        self.assertEqual(user_new("user new Janina, abcd, no", self.users_list, "test_users.json"), result)
        self.assertEqual(user_new("user new Zdzislaw, bcde, yes", self.users_list, "test_users.json"), result)

    def test_user_log(self):
        result = f"{self.first_user}"
        self.assertEqual(user_log(f"user log {self.first_user}, {self.first_passw}", self.users_list_full), result)

    def test_user_messages(self):
        result = f"0 messages for user: {self.first_user}"
        self.assertEqual(user_messages(self.first_user, self.messages), result)

    def test_user_delete(self):
        result = f"Deleted user: {self.first_user}"
        self.assertEqual(user_delete(f"user delete {self.first_user}", self.filename, self.users_list_full, "yes"),
                         result)


class TestMessages(unittest.TestCase):

    def setUp(self):
        self.users_list = {}
        self.users_list_full = {"Janina": {"password": "abcd", "admin": "no"},
                                "Zdzislaw": {"password": "bcde", "admin": "yes"}}
        self.first_user = "Janina"
        self.first_passw = "abcd"
        self.second_user = "Zdzislaw"
        self.second_passw = "bcde"
        self.user_bad = "Xybyz"
        self.messages = {}
        self.filename = "test_users.json"

    def test_message_new(self):
        result = f"Message sent from {self.first_user} to {self.second_user}"
        self.assertEqual(
            message_new(f"message new {self.second_user}, Witaj w domu", self.first_user, self.users_list_full), result)

    def test_message_read(self):
        result = f"Message from {self.first_user}: Witaj w domu"
        self.assertEqual(message_read("message read 1", self.second_user), result)

    def test_messages_delete(self):
        result = f"Messages for user {self.second_user} has been removed"
        self.assertEqual(message_delete(f"messages delete {self.second_user}", self.first_user, "yes"), result)


class TestBadData(unittest.TestCase):

    def setUp(self):
        self.users_list = {}
        self.users_list_full = {"Janina": {"password": "abcd", "admin": "no"},
                                "Zdzislaw": {"password": "bcde", "admin": "yes"}}
        self.first_user = "Janina"
        self.first_passw = "abcd"
        self.second_user = "Zdzislaw"
        self.second_passw = "bcde"
        self.user_bad = "Xybyz"
        self.messages = {}
        self.filename = "test_users.json"
        self.user_none = "none"

    def test_bad_password(self):
        result = "Cannot log this user - bad password"
        self.assertEqual(user_log(f"user log {self.first_user}, {self.second_passw}", self.users_list_full), result)

    def test_user_log_not_exist(self):
        result = "Cannot log this user - not exists in database"
        self.assertEqual(user_log(f"user log {self.user_bad}, {self.first_passw}", self.users_list_full), result)

    def test_not_logged_user(self):
        result = "You need to log in to read messages"
        self.assertEqual(message_read("message read 1", self.user_none), result)

    def test_user_exists(self):
        result = "Error: User already exists."
        self.assertEqual(user_new("user new Janina, abcd, yes", self.users_list_full, "test_users.json"), result)


if __name__ == '__main__':
    unittest.main()
