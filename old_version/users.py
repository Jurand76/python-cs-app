import json


def load_users(filename):
    try:
        with open(filename, 'r') as file:
            print('Users list loaded')
            return json.load(file)
    except FileNotFoundError:
        print('Users list empty')
        return {}


def user_list(users_list):
    response = "Users: "
    for item in users_list:
        response = response + item + ', '
    return response


def save_users(filename, users_list):
    with open(filename, 'w') as file:
        json.dump(users_list, file)


def user_new(data, users_list, filename):
    try:
        parts = data[8:].split(',')
        username = parts[0].replace(" ", "")
        password = parts[1].replace(" ", "")
        admin = parts[2].replace(" ", "").lower()

        print('New user data entered: ')
        print('Part 0 - username - : ', parts[0].replace(" ", ""))
        print('Part 1 - password - : ', parts[1].replace(" ", ""))
        print('Part 2 - is admin - : ', parts[2].replace(" ", ""))

        # Check if the user already exists
        if username in users_list:
            response = "Error: User already exists."
        else:
            users_list[username] = {'password': password, 'admin': admin}
            save_users(filename, users_list)  # Save to the JSON file
            response = "User created successfully."
    except NotImplementedError:
        response = "Bad parameters of command - enter username, password, no/yes (admin privileges)"

    return response


def user_log(data, users_list):
    try:
        parts = data[8:].split(',')
        username = parts[0].replace(" ", "")
        password = parts[1].replace(" ", "")

        print('Logging user: ')
        print(f'Part 0 - username - : {parts[0].replace(" ", "")}')

        # Check if the user already exists
        if username in users_list:
            if users_list[username]['password'] == password:
                user_logged = username
                print(f'Username to log: {user_logged}')
                user_admin = users_list[username]['admin'].lower()
                response = user_logged
                print('Login successful')
            else:
                response = "Cannot log this user - bad password"
        else:
            response = f"Cannot log this user - not exists in database"
    except NotImplementedError:
        response = "Cannot log this user - bad parameters"

    return response


def user_delete(data, filename, users_list, user_admin):
    try:
        parts = data[11:]
        username = parts.replace(' ', '')

        print('Deleting user: ')
        print('Part 0 - username - : ', username)

        # Check if the user already exists and if logged user has active admin privileges
        if username in users_list:
            if user_admin == 'yes':
                del users_list[username]
                save_users(filename, users_list)
                response = f"Deleted user: {username}"
                print('User delete successful')
            else:
                response = "You need administrator privileges to delete users"
        else:
            response = f"User {username} not exists in database"
    except NotImplementedError:
        response = "Bad parameters of command"

    return response


def user_messages_load(username):
    if username == 'none':
        response = "No logged user"
    else:
        msg = {}
        try:
            filename = username + '.json'
            with open(filename, 'r') as file:
                print('Message file loaded')
                msg = json.load(file)
        except FileNotFoundError:
            print('No message file')
            msg = {}
    return msg


def user_messages(user_logged, messages):
    if user_logged == 'none':
        response = "No logged user"
    else:
        response = f"{len(messages)} messages for user: {user_logged}"
    return response
